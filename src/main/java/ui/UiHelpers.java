package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import lombok.val;

import java.util.function.Consumer;

public class UiHelpers {
    public static EventHandler<ActionEvent> toSafeHandler(UnsafeEventHandler<ActionEvent> eventHandler) {
        return event -> {
            try {
                eventHandler.handle(event);
            } catch (Exception e) {
                showError(e.getMessage());
                e.printStackTrace();
            }
        };
    }

    public static void showError(String message) {
        val alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error!");
        alert.setHeaderText("Error!");
        alert.setContentText(message);

        alert.showAndWait();
    }

    public static void showInfo(String message) {
        val alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText("Info");
        alert.setContentText(message);

        alert.showAndWait();
    }

    public static Button createButton(String label, UnsafeEventHandler<ActionEvent> eventHandler) {
        val btn = new Button(label);
        btn.setOnAction(toSafeHandler(eventHandler));
        return btn;
    }

    public static TextField createInput(String value, Consumer<String> onBlur) {
        val input = new TextField(value);
        input.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) { // loose focus
                onBlur.accept(input.getText());
            }
        });
        return input;
    }

    public static TextField createIntInput(int value, Consumer<Integer> onBlur) {
        return createInput(Integer.toString(value), newValue -> {
            try {
                onBlur.accept(Integer.parseInt(newValue));
            } catch (NumberFormatException e) {
                showError("Invalid integer format.");
            } catch (IllegalArgumentException e) {
                showError(e.getMessage());
            }
        });
    }

    public static TextField createDoubleInput(double value, Consumer<Double> onBlur) {
        return createInput(Double.toString(value), newValue -> {
            try {
                onBlur.accept(Double.parseDouble(newValue));
            } catch (NumberFormatException e) {
                showError("Invalid double format.");
            }
        });
    }

    public static CheckBox createCheckbox(boolean value, String label, Consumer<Boolean> onChange) {
        val checkBox = new CheckBox(label);
        checkBox.setSelected(value);
        checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            onChange.accept(newValue);
        });
        return checkBox;
    }

    public static Pane createFlowPane(Node ...children) {
        FlowPane rootPanel = new FlowPane();
        rootPanel.setPadding(new Insets(5, 0, 5, 0));
        rootPanel.setVgap(4);
        rootPanel.setHgap(4);
        rootPanel.getChildren().addAll(children);
        return rootPanel;
    }
}
