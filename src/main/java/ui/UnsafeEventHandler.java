package ui;

@FunctionalInterface
public interface UnsafeEventHandler<T> {
    void handle(T t) throws Exception;
}
