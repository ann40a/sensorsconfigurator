package ui;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import lombok.val;

/*
* Builder pattern implementation:
* It is a bad practice to have methods/constructors with more than 3 arguments.
* But javafx.scene.Scene constructors have up to 6 arguments.
* SceneBuilder is intended to solve this and allow to configure Scene in more readable
* and less error prone manner.
*/
public class SceneBuilder {
    private Parent root;
    private double width = -1;
    private double height = -1;
    private Paint fill = Color.WHITE;
    private boolean depthBuffer = false;
    private SceneAntialiasing antiAliasing = SceneAntialiasing.DISABLED;

    public SceneBuilder setRoot(Parent root) {
        this.root = root;
        return this;
    }

    public SceneBuilder setWidth(double width) {
        this.width = width;
        return this;
    }

    public SceneBuilder setHeight(double height) {
        this.height = height;
        return this;
    }

    public SceneBuilder setFill(Paint fill) {
        this.fill = fill;
        return this;
    }

    public SceneBuilder setDepthBuffer(boolean depthBuffer) {
        this.depthBuffer = depthBuffer;
        return this;
    }

    public SceneBuilder setAntiAliasing(SceneAntialiasing antiAliasing) {
        this.antiAliasing = antiAliasing;
        return this;
    }

    public Scene build() {
        val scene = new Scene(
                root,
                width,
                height,
                depthBuffer,
                antiAliasing
        );
        scene.setFill(fill);
        return scene;
    }
}
