package sensors;

import lombok.Data;

@Data
public abstract class Sensor {
    protected double scaleFactor = 1.0;
}
