package sensors.loadable;

import ui.UiHelpers;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import sensors.Sensor;
import sensors.SensorPlugin;

public class SpeedSensorPlugin extends SensorPlugin<SpeedSensor> {
    @Override
    public Class<? extends Sensor> getSensorClass() {
        return SpeedSensor.class;
    }

    @Override
    public SpeedSensor createNewSensor() {
        return new SpeedSensor(150, 5);
    }

    @Override
    public Pane initializeControls(SpeedSensor sensor) {
        return UiHelpers.createFlowPane(
                new Label("Max value"),
                UiHelpers.createIntInput(sensor.getMaxValue(), sensor::setMaxValue),
                new Label("Max error"),
                UiHelpers.createIntInput(sensor.getMaxError(), sensor::setMaxError)
        );
    }
}
