package sensors.loadable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sensors.Sensor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SpeedSensor extends Sensor {
    protected int maxValue;
    protected int maxError;
}
