package sensors.loadable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sensors.Sensor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class AssessmentsSensor extends Sensor {
    private int maxAssessment;

    public void setMaxAssessment(int maxAssessment) {
        if (maxAssessment <= 0) {
            throw new IllegalArgumentException("Max assessment should be > 0");
        }
        this.maxAssessment = maxAssessment;
    }
}
