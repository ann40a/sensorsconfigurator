package sensors.loadable;

import ui.UiHelpers;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import sensors.Sensor;
import sensors.SensorPlugin;

public class AssessmentsSensorPlugin extends SensorPlugin<AssessmentsSensor> {
    @Override
    public Class<? extends Sensor> getSensorClass() {
        return AssessmentsSensor.class;
    }

    @Override
    public AssessmentsSensor createNewSensor() {
        return new AssessmentsSensor(5);
    }

    @Override
    public Pane initializeControls(AssessmentsSensor sensor) {
        return UiHelpers.createFlowPane(
                new Label("Max Assessment"),
                UiHelpers.createIntInput(sensor.getMaxAssessment(), sensor::setMaxAssessment)
        );
    }
}
