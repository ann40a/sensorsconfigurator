package sensors.impl;

import lombok.*;
import sensors.Sensor;

import java.util.stream.Stream;

@Data
@EqualsAndHashCode(callSuper = true, exclude = { "checksum" })
@ToString(exclude = { "checksum" })
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TemperatureSensor extends Sensor {
    private int minValue;
    private int maxValue;
    private TemperatureUnit unit = TemperatureUnit.CELSIUS;
    private String checksum;

    public TemperatureSensor(int minValue, int maxValue, TemperatureUnit unit) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.unit = unit;
    }

    public enum TemperatureUnit {
        CELSIUS('C'), FAHRENHEIT('F'), KELVIN('K');

        public char symbol;

        TemperatureUnit(char symbol) {
            this.symbol = symbol;
        }

        public static TemperatureUnit fromSymbol(char symbol) {
            return Stream.of(values())
                    .filter(s -> s.symbol == symbol)
                    .findAny()
                    .orElseThrow(NullPointerException::new);
        }
    }
}
