package sensors.impl;

import ui.UiHelpers;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import sensors.Sensor;
import sensors.SensorPlugin;

public class AtmosphericPressureSensorPlugin extends SensorPlugin<AtmosphericPressureSensor> {
    @Override
    public Class<? extends Sensor> getSensorClass() {
        return AtmosphericPressureSensor.class;
    }

    @Override
    public AtmosphericPressureSensor createNewSensor() {
        return new AtmosphericPressureSensor(0, 800);
    }

    @Override
    public Pane initializeControls(AtmosphericPressureSensor sensor) {
        return UiHelpers.createFlowPane(
                new Label("Min value"),
                UiHelpers.createIntInput(sensor.getMinValue(), sensor::setMinValue),
                new Label("Max value"),
                UiHelpers.createIntInput(sensor.getMaxValue(), sensor::setMaxValue)
        );
    }
}
