package sensors.impl;

import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import lombok.val;
import sensors.Sensor;
import sensors.SensorPlugin;
import sensors.SensorPreprocessor;
import ui.UiHelpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TemperatureSensorPlugin extends SensorPlugin<TemperatureSensor> {
    private static List<SensorPreprocessor<TemperatureSensor>> sensorPreprocessors = new ArrayList<>();
    private static final String CHECKSUM_ALGORITHM = "SHA-1";
    private static MessageDigest messageDigest;

    static {
        try {
            messageDigest = MessageDigest.getInstance(CHECKSUM_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        sensorPreprocessors.add(
            new SensorPreprocessor<>(
                    "Generate & save data checksum",
                    sensor -> {
                        sensor.setChecksum(TemperatureSensorPlugin.calculateChecksum(sensor));
                        return sensor;
                    },
                    sensor -> {
                        val expectedCheckSum = TemperatureSensorPlugin.calculateChecksum(sensor);
                        if (!expectedCheckSum.equals(sensor.getChecksum())) {
                            throw new IllegalStateException("Temperature sensor checksum doesn't match.");
                        }
                        return sensor;
                    }
            )
        );
    }

    @Override
    public Class<? extends Sensor> getSensorClass() {
        return TemperatureSensor.class;
    }

    @Override
    public TemperatureSensor createNewSensor() {
        return new TemperatureSensor(-50, 40, TemperatureSensor.TemperatureUnit.CELSIUS);
    }

    @Override
    public Pane initializeControls(TemperatureSensor sensor) {
        return UiHelpers.createFlowPane(
                new Label("Min value"),
                UiHelpers.createIntInput(sensor.getMinValue(), sensor::setMinValue),
                new Label("Max value"),
                UiHelpers.createIntInput(sensor.getMaxValue(), sensor::setMaxValue),
                new Label("Unit"),
                createTemperatureUnitInput(sensor)
        );
    }

    @Override
    public List<SensorPreprocessor<TemperatureSensor>> getPreprocessors() {
        return sensorPreprocessors;
    }

    private ChoiceBox<Character> createTemperatureUnitInput(TemperatureSensor sensor) {
        val symbols = Stream.of(TemperatureSensor.TemperatureUnit.values())
                .map(u -> u.symbol)
                .collect(Collectors.toList());
        val sensorClassInput = new ChoiceBox<Character>(
                FXCollections.observableArrayList(symbols)
        );
        if (sensor.getUnit() != null) {
            sensorClassInput.setValue(sensor.getUnit().symbol);
        }
        sensorClassInput.setOnAction(event -> {
            sensor.setUnit(TemperatureSensor.TemperatureUnit.fromSymbol(sensorClassInput.getValue()));
        });
        return sensorClassInput;
    }

    private static String calculateChecksum(TemperatureSensor sensor) {
        val sensorData = sensor.toString();
        System.out.println(sensorData);
        val mdBytes = messageDigest.digest(sensorData.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte mdByte : mdBytes) {
            sb.append(Integer.toString((mdByte & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return sb.toString();
    }
}
