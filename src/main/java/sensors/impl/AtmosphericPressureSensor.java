package sensors.impl;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sensors.Sensor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class AtmosphericPressureSensor extends Sensor {
    protected int minValue;
    protected int maxValue;
}
