package sensors;

import javafx.scene.layout.Pane;

import java.util.List;

public abstract class SensorPlugin<T extends Sensor> {
    public abstract Class<? extends Sensor> getSensorClass();

    public abstract T createNewSensor();

    public abstract Pane initializeControls(T sensor);

    public List<SensorPreprocessor<T>> getPreprocessors() {
        return null;
    }
}
