package sensors.adapted;

import javafx.scene.Node;
import sensors.Sensor;

public abstract class ExternalSensorPlugin<T extends Sensor> {
    public abstract Class<? extends Sensor> getExternalSensorClass();

    public abstract T createNewExternalSensor();

    public abstract Node[] createPaneWithControls(T sensor);
}
