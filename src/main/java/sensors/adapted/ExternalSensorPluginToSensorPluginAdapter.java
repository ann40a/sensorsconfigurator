package sensors.adapted;

import ui.UiHelpers;
import javafx.scene.layout.Pane;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import sensors.Sensor;
import sensors.SensorPlugin;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExternalSensorPluginToSensorPluginAdapter<T extends Sensor> extends SensorPlugin<T> {
    private ExternalSensorPlugin<T> sensorPlugin;

    @Override
    public Class<? extends Sensor> getSensorClass() {
        return sensorPlugin.getExternalSensorClass();
    }

    @Override
    public T createNewSensor() {
        return sensorPlugin.createNewExternalSensor();
    }

    @Override
    public Pane initializeControls(T sensor) {
        return UiHelpers.createFlowPane(
                sensorPlugin.createPaneWithControls(sensor)
        );
    }
}
