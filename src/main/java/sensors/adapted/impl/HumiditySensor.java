package sensors.adapted.impl;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import sensors.Sensor;

import java.util.stream.Stream;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class HumiditySensor extends Sensor {
    private SensorLocation location;

    @AllArgsConstructor
    public enum SensorLocation {
        INSIDE("Inside"), OUTSIDE("Outside");

        public String location;

        public static SensorLocation fromLocation(String location) {
            return Stream.of(values())
                    .filter(s -> s.location.equals(location))
                    .findAny()
                    .orElseThrow(NullPointerException::new);
        }
    }
}
