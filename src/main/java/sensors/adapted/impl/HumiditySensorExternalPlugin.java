package sensors.adapted.impl;

import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import lombok.val;
import sensors.Sensor;
import sensors.adapted.ExternalSensorPlugin;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HumiditySensorExternalPlugin extends ExternalSensorPlugin<HumiditySensor> {
    @Override
    public Class<? extends Sensor> getExternalSensorClass() {
        return HumiditySensor.class;
    }

    @Override
    public HumiditySensor createNewExternalSensor() {
        return new HumiditySensor(HumiditySensor.SensorLocation.INSIDE);
    }

    @Override
    public Node[] createPaneWithControls(HumiditySensor sensor) {
        return new Node[] {
                new Label("Location"),
                createLocationInput(sensor)
        };
    }

    private ChoiceBox<String> createLocationInput(HumiditySensor sensor) {
        val symbols = Stream.of(HumiditySensor.SensorLocation.values())
                .map(u -> u.location)
                .collect(Collectors.toList());
        val sensorClassInput = new ChoiceBox<String>(
                FXCollections.observableArrayList(symbols)
        );
        if (sensor.getLocation() != null) {
            sensorClassInput.setValue(sensor.getLocation().location);
        }
        sensorClassInput.setOnAction(event -> {
            sensor.setLocation(HumiditySensor.SensorLocation.fromLocation(sensorClassInput.getValue()));
        });
        return sensorClassInput;
    }
}
