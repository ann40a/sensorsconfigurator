package sensors;

import lombok.Getter;
import lombok.Setter;

import java.util.function.Function;

public class SensorPreprocessor<T extends Sensor> {
    @Getter
    private final String name;

    @Getter @Setter
    private boolean isEnabled = false;

    private final Function<T, T> onBeforeSerialize;
    private final Function<T, T> onAfterDeserialize;

    public SensorPreprocessor(
            String name,
            Function<T, T> onBeforeSerialize,
            Function<T, T> onAfterDeserialize
    ) {
        this.name = name;
        this.onBeforeSerialize = onBeforeSerialize;
        this.onAfterDeserialize = onAfterDeserialize;
    }

    public Sensor processBeforeSerialize(T sensor) {
        if (isEnabled && onBeforeSerialize != null) {
            return onBeforeSerialize.apply(sensor);
        }
        return sensor;
    }

    public Sensor processAfterDeserialize(T sensor) {
        if (isEnabled && onAfterDeserialize != null) {
            return onAfterDeserialize.apply(sensor);
        }
        return sensor;
    }
}
