import core.PluginLoader;
import core.SensorSerializer;
import lombok.Getter;
import lombok.val;
import sensors.Sensor;
import sensors.SensorPlugin;
import sensors.adapted.ExternalSensorPluginToSensorPluginAdapter;
import sensors.adapted.impl.HumiditySensorExternalPlugin;
import sensors.impl.AtmosphericPressureSensorPlugin;
import sensors.impl.TemperatureSensorPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 * Singleton pattern implementation:
 * Application state holder class.
 * It is responsible for all operations with sensors.
 */
public class SensorsConfiguratorAppState {
    @Getter
    private static SensorsConfiguratorAppState instance = new SensorsConfiguratorAppState();
    @Getter
    private List<Sensor> sensors = new ArrayList<>();
    @Getter
    private List<SensorPlugin<? extends Sensor>> sensorPlugins;

    private PluginLoader pluginLoader = new PluginLoader();
    private SensorSerializer serializer;
    private Map<String, SensorPlugin> sensorPluginsMap;

    public List<String> getSensorClassesNames() {
        return sensorPlugins.stream()
                .map(SensorPlugin::getSensorClass)
                .map(Class::getCanonicalName)
                .collect(Collectors.toList());
    }

    public void loadSensors(File file) throws IOException {
        sensors = serializer.deserializeFromFile(file.getAbsolutePath())
            .stream()
            .map(this::applyAfterDeserializePreprocessors)
            .collect(Collectors.toList());
    }

    public void saveSensors(File file) throws IOException {
        val processedSensors = sensors.stream()
                .map(this::applyBeforeSerializePreprocessors)
                .collect(Collectors.toList());
        serializer.serializeToFile(file.getAbsolutePath(), processedSensors);
    }

    public void createSensor(String sensorClass) {
        val newSensor = sensorPluginsMap.get(sensorClass).createNewSensor();
        sensors.add(newSensor);
    }

    public void loadPlugin(File file) throws Exception {
        val newPlugin = pluginLoader.loadFromFile(file);
        sensorPlugins.add(newPlugin);
        sensorPluginsMap.put(newPlugin.getSensorClass().getCanonicalName(), newPlugin);
        serializer = new SensorSerializer(sensorPlugins);
    }

    public SensorPlugin<Sensor> getSensorPlugin(Sensor sensor) {
        return sensorPluginsMap.get(sensor.getClass().getCanonicalName());
    }

    private SensorsConfiguratorAppState() {
        initDefaultPlugins();
    }

    private void initDefaultPlugins() {
        sensorPlugins = new ArrayList<>();
        sensorPlugins.add(new AtmosphericPressureSensorPlugin());
        sensorPlugins.add(new TemperatureSensorPlugin());
        sensorPlugins.add(new ExternalSensorPluginToSensorPluginAdapter<>(new HumiditySensorExternalPlugin()));

        sensorPluginsMap = sensorPlugins.stream().collect(Collectors.toMap(
                pl -> pl.getSensorClass().getCanonicalName(),
                pl -> pl
        ));

        serializer = new SensorSerializer(sensorPlugins);
    }

    private Sensor applyBeforeSerializePreprocessors(Sensor sensor) {
        val sensorPlugin = getSensorPlugin(sensor);
        val preprocessors = sensorPlugin.getPreprocessors();
        Sensor processedSensor = sensor;
        if (preprocessors != null) {
            for (val processor : preprocessors) {
                processedSensor = processor.processBeforeSerialize(sensor);
            }
        }
        return processedSensor;
    }

    private Sensor applyAfterDeserializePreprocessors(Sensor sensor) {
        val sensorPlugin = getSensorPlugin(sensor);
        val preprocessors = sensorPlugin.getPreprocessors();
        Sensor processedSensor = sensor;
        if (preprocessors != null) {
            for (val processor : preprocessors) {
                processedSensor = processor.processAfterDeserialize(sensor);
            }
        }
        return processedSensor;
    }
}
