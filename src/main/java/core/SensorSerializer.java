package core;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.undercouch.bson4jackson.BsonFactory;
import lombok.*;
import sensors.Sensor;
import sensors.SensorPlugin;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class SensorSerializer {
    private static ObjectMapper mapper = new ObjectMapper(new BsonFactory());
    private static TypeReference sensorsCollectionType = new TypeReference<List<SensorDeserializationWrapper>>() {};

    private List<? extends Class<? extends Sensor>> sensorClasses;

    public SensorSerializer(List<SensorPlugin<? extends Sensor>> sensorPlugins) {
        sensorClasses = sensorPlugins.stream()
                .map(SensorPlugin::getSensorClass)
                .collect(Collectors.toList());
    }

    @Getter
    @AllArgsConstructor
    static private class SensorSerializationWrapper {
        private Sensor sensor;

        public String getCanonicalClassName() {
            return sensor.getClass().getCanonicalName();
        }
    }

    @Setter
    @NoArgsConstructor
    static private class SensorDeserializationWrapper {
        private String canonicalClassName;
        private Map<String, Object> sensor;

        public Sensor toSensor(List<? extends Class<? extends Sensor>> sensorClasses) {
            val sensorClass = sensorClasses.stream()
                    .filter(s -> Objects.equals(s.getCanonicalName(), canonicalClassName))
                    .findAny()
                    .orElseThrow(NullPointerException::new);
            return mapper.convertValue(sensor, sensorClass);
        }
    }

    public void serializeToFile(String filePath, List<Sensor> sensors) throws IOException {
        val wrappedSensors = sensors.stream()
                .map(SensorSerializationWrapper::new)
                .collect(Collectors.toList());

        try(OutputStream outputStream = new FileOutputStream(new File(filePath))) {
            mapper.writeValue(outputStream, wrappedSensors);
        }
    }

    public List<Sensor> deserializeFromFile(String filePath) throws IOException {
        List<SensorDeserializationWrapper> wrappedSensors;
        try(InputStream inputStream = new FileInputStream(new File(filePath))) {
            wrappedSensors = mapper.readValue(inputStream, sensorsCollectionType);
        }
        return wrappedSensors.stream()
                .map(sw -> sw.toSensor(sensorClasses))
                .collect(Collectors.toList());
    }
}
