package core;

import lombok.val;
import sensors.Sensor;
import sensors.SensorPlugin;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginLoader {
    private static final String PACKAGE_PREFIX = "sensors.loadable.";

    @SuppressWarnings("unchecked")
    public SensorPlugin<? extends Sensor> loadFromFile(File file) throws Exception {
        val url = file.toURI().toURL();
        val cl = new URLClassLoader(new URL[]{url});
        val cls = cl.loadClass(getClassNameForFile(file));

        return (SensorPlugin<? extends Sensor>) cls.newInstance();
    }

    private String getClassNameForFile(File file) {
        val fileName = file.getName();
        return PACKAGE_PREFIX + fileName.substring(0, fileName.length() - ".class".length());
    }
}
