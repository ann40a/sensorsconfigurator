import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.val;
import sensors.Sensor;
import sensors.SensorPlugin;
import ui.SceneBuilder;
import ui.UiHelpers;

import java.io.File;
import java.util.stream.Stream;

import static ui.UiHelpers.createButton;

public class SensorsConfigurator extends Application {
    private SensorsConfiguratorAppState appState = SensorsConfiguratorAppState.getInstance();
    private Stage primaryStage;
    private Pane rootPanel;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Sensors Configurator");
        primaryStage.setScene(
                new SceneBuilder()
                        .setRoot(initRootPanel())
                        .setWidth(800)
                        .setHeight(500)
                        .setFill(Color.BLUE)
                        .build()
        );

        primaryStage.show();
    }

    private Pane initRootPanel() {
        val rootPanel = new VBox();
        rootPanel.setPadding(new Insets(5, 0, 5, 0));
        rootPanel.setSpacing(8);
        rootPanel.getChildren().addAll(getRootNodes());

        this.rootPanel = rootPanel;
        return rootPanel;
    }

    private Node[] getRootNodes() {
        return Stream.of(
                new Pane[] {
                        createActionsPane(),
                        createAddNewSensorPane()
                },
                createSensorsPreprocessorsPanes(),
                createSensorsPanes()
        )
                .flatMap(Stream::of)
                .toArray(Pane[]::new);
    }

    private Pane createActionsPane() {
        val fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        return UiHelpers.createFlowPane(
                createButton("Load from file", actionEvent -> {
                    File file = fileChooser.showOpenDialog(primaryStage);

                    if (file != null) {
                        appState.loadSensors(file);
                        uiDataChanged();
                    }
                }),
                createButton("Save to file", actionEvent -> {
                    File file = fileChooser.showSaveDialog(primaryStage);

                    if(file != null){
                        appState.saveSensors(file);
                        UiHelpers.showInfo("Successfully saved to file!");
                    }
                }),
                createButton("Load plugin from file", actionEvent -> {
                    val classFileChooser = new FileChooser();
                    FileChooser.ExtensionFilter classExtFilter = new FileChooser.ExtensionFilter("Java class files (*.class)", "*.class");
                    classFileChooser.getExtensionFilters().add(classExtFilter);
                    File file = classFileChooser.showOpenDialog(primaryStage);

                    if (file != null) {
                        appState.loadPlugin(file);
                        uiDataChanged();
                    }
                })
        );
    }

    private Pane createAddNewSensorPane() {
        val sensorClassesNames = appState.getSensorClassesNames();
        val sensorClassInput = new ChoiceBox<String>(
                FXCollections.observableArrayList(sensorClassesNames)
        );
        return UiHelpers.createFlowPane(
                sensorClassInput,
                createButton("Add", actionEvent -> {
                    appState.createSensor(sensorClassInput.getValue());
                    uiDataChanged();
                })
        );
    }

    private void uiDataChanged() {
        rootPanel.getChildren().setAll(getRootNodes());
    }

    private Pane[] createSensorsPanes() {
        return appState.getSensors().stream()
                .map(this::createSensorPane)
                .toArray(Pane[]::new);
    }

    private Pane[] createSensorsPreprocessorsPanes() {
        return appState.getSensorPlugins().stream()
                .filter(pr -> pr.getPreprocessors() != null)
                .map(this::createSensorPreprocessorsPane)
                .toArray(Pane[]::new);
    }

    private Pane createSensorPane(Sensor sensor) {
        val sensorPlugin = appState.getSensorPlugin(sensor);
        val pane = sensorPlugin.initializeControls(sensor);
        pane.getChildren().addAll(
                new Label("Scale Factor"),
                UiHelpers.createDoubleInput(
                        sensor.getScaleFactor(),
                        sensor::setScaleFactor
                ),
                UiHelpers.createButton("Remove", e -> {
                    appState.getSensors().remove(sensor);
                    uiDataChanged();
                })
        );
        return pane;
    }

    private Pane createSensorPreprocessorsPane(SensorPlugin<? extends Sensor> sensorPlugin) {
        val pane = UiHelpers.createFlowPane(
                new Label(sensorPlugin.getSensorClass().getCanonicalName() + ": ")
        );
        val children = pane.getChildren();
        sensorPlugin.getPreprocessors().forEach(pr -> {
            children.add(UiHelpers.createCheckbox(
                    pr.isEnabled(),
                    pr.getName(),
                    pr::setEnabled
            ));
        });
        return pane;
    }
}
