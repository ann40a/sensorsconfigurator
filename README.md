## Perspective technologies of object-oriented programming
### Labs 3 - 6 implementation notes
SensorsConfigurator is a program that helps engineers to configure different types of sensor. For example, if you have temperature sensor, you can specify its minimum and maximum value, temperature unit, and scale factor (scale factor is common for all sensors).  
[Screenshot](https://prnt.sc/izms8s)

## Lab3 - Objects serialization
Output format: BSON  
Responsible class: core.SensorSerializer  
[Screenshot](https://prnt.sc/izmv67)

## Lab4 - Plugins hierarchy
All plugins in package sensors.loadable are not used directly in the program and intended for dynamic loading from UI.

## Lab5 - Plugins functionality
sensors.impl.TemperatureSensorPlugin contains pre and post processors that can be enabled or disabled from UI.  
According to the variant implemented preprocessor generates checksum for plugin data.  
[Screenshot](http://prntscr.com/izmveo)

## Lab6 - Patterns
1. Adapter - sensors.adapted.ExternalSensorPluginToSensorPluginAdapter
2. Builder - ui.SceneBuilder
3. Singleton - SensorsConfiguratorAppState
